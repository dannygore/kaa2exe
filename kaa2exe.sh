#!/bin/bash
echo "=--------- kaa-editor-install-and-freeze ------="
echo "todo: delete also PIP package if not previously installed"
echo "todo: make modification, e.g. alt+f to open the menu, ctrl+f to find text"


# don't use "kaa" : it's the module's name
tmp=/tmp/tmpkaadir
f=tempkaafname.py   

echo "=--------- install kaa pre-req: ------"
if dpkg -s libncursesw5 ;     then l1=true; else l1=false; sudo apt install libncursesw5; fi
if dpkg -s libncurses5-dev ;  then l2=true; else l2=false; sudo apt install libncurses5-dev; fi
if dpkg -s libncursesw5-dev ; then l3=true; else l3=false; sudo apt install libncursesw5-dev; fi
# sudo apt-get -y install libncursesw5 libncurses5-dev libncursesw5-dev

pip install kaaedit
pip install PyInstaller
mkdir $tmp
pushd $tmp

cp `which kaa` $f
echo >> $f
echo import kaa >> $f   # let pyinstaller recognize kaa module, just as a precaution

echo "=----- convert to executable ----"
pyinstaller $f -n tempbin --onefile
cp dist/tempbin kaaeditor

echo "=----- install to usr/local/bin -----"
sudo cp kaaeditor /usr/local/bin/

echo "=-----  cleanup ----"
# delete packges which were NOT installed before
if ! $l1 ; then sudo apt-get -y purge libncursesw5 ; fi
if ! $l2 ; then sudo apt-get -y purge libncurses5-dev ; fi
if ! $l3 ; then sudo apt-get -y purge libncursesw5-dev ; fi


echo done.
echo installed to /usr/local/bin/kaaeditor
popd

