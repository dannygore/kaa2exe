# kaa2exe
Script to create a Kaa editor in self-contained, single file, executable binary.
It downloads latest version (via pip), temporarily install prereqs for compilation, create the binary (~7mb), install to usr-local-bin, cleanup.

# Purpose: 
run kaa also on servers that don't or can't have the pre-requists (e.g. apt install, pip install, or even python3)

# w2hat is KaaEdit 
A console-mode text editor (CUI, e.g. for servers), using Windows shortcuts and accelerator keys and a menu system.

For Kaa sources, install instructions (cavit!), usage and kudus:
https://github.com/kaaedit/kaa

 * improvements and pull requests are welcome.
 * I don't see a point in a Windows version (tried, it's nice, but pretty much pointless.)
 * was tested on ubuntu 64bit

# todo
copy my key-binding file, into the library,  after pip install, before py2exe.
Compile a release here on gitlab using CI 
